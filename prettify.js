// Add any folder or files in array, if you want it to be formatted
const folders = [
    'src/**/*.ts',
    'src/**/*.html',
    'src/**/*.json',
    'src/**/*.scss',
];

async function prettier() {
    const { stdout } = await require('util').promisify(require('child_process').exec)(
        'npx prettier --write'
            .split(' ')
            .concat(folders)
            .join(' '),
        {
            cwd: process.cwd()
        }
    );

    console.log(stdout);
}

prettier();
