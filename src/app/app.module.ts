import { LayoutModule } from '@angular/cdk/layout';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MdcComponent } from './mdc/mdc.component';
import { GitlabService } from './service/gitlab.service';
import { AngularMdcModule } from './shared/angular-mdc.module';
import { MaterialModule } from './shared/material.module';

@NgModule({
    declarations: [AppComponent, LoginComponent, MdcComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
        BrowserAnimationsModule,
        LayoutModule,
        AngularMdcModule,
        MaterialModule,
    ],
    providers: [GitlabService],
    bootstrap: [AppComponent],
})
export class AppModule {}
